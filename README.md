# Only for Johan

Jangan lupa cekidot [panduan bikin resume & cover letter](https://gist.github.com/iamdejan/c67ae3219388cf03e5b2e0f18bd7952a).

## Indonesia

- [Bank Jago - One Day Recruitment Program - Senior Software Engineer - Jakarta](https://www.linkedin.com/jobs/view/2786332321/?refId=1m23MAOyaS6Wz%2FHO8RrP3g%3D%3D&trackingId=M2eufyl0fwDGpQB7%2BNJfyQ%3D%3D&trk=d_flagship3_company)
- [Kata.ai - Platform Engineer - Jakarta / Banten](https://www.linkedin.com/jobs/view/2802196498/?refId=sSqZOZdht%2B0vYzj7NItWrA%3D%3D&trackingId=4xTMZzvEQBEGdpqZB1HkWw%3D%3D&trk=d_flagship3_company)
    - hybrid pas kerja
- [VERIHUBS - Senior Software Engineer - Jakarta](https://www.linkedin.com/jobs/view/2729179519/?refId=TCAX4Kzmt6xYADe5gPy21w%3D%3D&trackingId=ATvycyuLbCP1TDt%2BeT5zfQ%3D%3D&trk=d_flagship3_company)
    - hybrid pas kerja
- [SIRCLO - Back End Engineer - The Breeze, BSD](https://www.linkedin.com/jobs/view/2803284574/?refId=HIAniiOrnJr%2B6O%2FMebyAPw%3D%3D&trackingId=J9mGCkQ4%2FytJ3huSKC0WEQ%3D%3D&trk=d_flagship3_company)
- [BukuWarung - Software Engineer - Jakarta](https://www.linkedin.com/jobs/view/2805501855/?refId=6e5f312a-42a6-41fe-be00-a2ba01c92c9f&trk=flagship3_job_home_savedjobs)
- [Bukalapak - Backend Engineer - Growth - Jakarta](https://www.linkedin.com/jobs/view/2793407929/?refId=4994d962-6e5a-4806-92b0-b78a6a115ee3&trk=flagship3_job_home_savedjobs)
- [Bukalapak Software Engineer, Backend - Core - Jakarta](https://www.linkedin.com/jobs/view/2803964078/?refId=e3b4fde8-8208-42c9-913e-713b91260669&trk=flagship3_job_home_savedjobs)
    - hybrid pas kerja
- [Accenture - Java Architect - Jakarta](https://www.linkedin.com/jobs/view/2664478050/?refId=4994d962-6e5a-4806-92b0-b78a6a115ee3&trk=flagship3_job_home_savedjobs)
- [Indodana - Software Engineer (Freshgraduate, Senior, Principal, Architect Position) - Jakarta](https://www.linkedin.com/jobs/view/2785663405/?refId=6e5f312a-42a6-41fe-be00-a2ba01c92c9f&trk=flagship3_job_home_savedjobs)
- [Majoo - Back End Developer (Golang) - Malang](https://www.linkedin.com/jobs/view/2800182601/?refId=h8yNK998tGQV0Uglm9D1CQ%3D%3D&trackingId=QNngAdRaVXTddGIMftDHow%3D%3D)
- [Antikode - Blockchain Developer - Bintaro, Tangerang Selatan](https://www.linkedin.com/jobs/view/2790048445/?refId=caed2b32-0c7b-43d1-9e34-a51833819b87&trk=flagship3_job_home_savedjobs)
- [GudangAda - Software Engineer – Backend - Gading Serpong](https://www.linkedin.com/jobs/view/2794216893/?refId=caed2b32-0c7b-43d1-9e34-a51833819b87&trk=flagship3_job_home_savedjobs)
- [Sayurbox - Software Development Engineer - Jakarta](https://www.linkedin.com/jobs/view/2772742800/?refId=caed2b32-0c7b-43d1-9e34-a51833819b87&trk=flagship3_job_home_savedjobs)
- [Detik.com - Back End Developer - Jakarta](https://www.linkedin.com/jobs/view/2794275467/?refId=caed2b32-0c7b-43d1-9e34-a51833819b87&trk=flagship3_job_home_savedjobs)
    - on-site pas kerja
- [BRI - Back End Developer - Jakarta](https://www.linkedin.com/jobs/view/2789646751/?refId=42b2198d-b1f7-46ef-8fc3-22b4a5e2e507&trk=flagship3_job_home_savedjobs)
    - hybrid pas kerja
- [Janji Jiwa Group (Kopi Janji Jiwa) - Back End Developer - Jakarta](https://www.linkedin.com/jobs/view/2665524474/?refId=42b2198d-b1f7-46ef-8fc3-22b4a5e2e507&trk=flagship3_job_home_savedjobs)
    - hybrid pas kerja
- [Blue Bird Group - Back End Developer - Jakarta](https://www.linkedin.com/jobs/view/2777279029/?refId=42b2198d-b1f7-46ef-8fc3-22b4a5e2e507&trk=flagship3_job_home_savedjobs)
    - hybrid pas kerja
- [Saham Rakyat - Back End Developer - Jakarta](https://www.linkedin.com/jobs/view/2777941479/?refId=42b2198d-b1f7-46ef-8fc3-22b4a5e2e507&trk=flagship3_job_home_savedjobs)
- [Bibit.id - Backend Engineer (NodeJS) - Jakarta](https://www.linkedin.com/jobs/view/2783670450/?refId=42b2198d-b1f7-46ef-8fc3-22b4a5e2e507&trk=flagship3_job_home_savedjobs)
- [Smartfren - Software Engineer - BSD](https://www.linkedin.com/jobs/view/2758188270/?refId=54f949c9-dac2-47eb-baaa-1a03f4702dba&trk=flagship3_job_home_savedjobs)
    - on-site pas kerja
- [Ajaib - Senior Back End Developer - Jakarta](https://www.linkedin.com/jobs/view/2642709558/?refId=54f949c9-dac2-47eb-baaa-1a03f4702dba&trk=flagship3_job_home_savedjobs)
- [Kargo Technologies - Backend Engineer - Jakarta](https://www.linkedin.com/jobs/view/2787248667/?refId=6416e2d2-13db-4d92-b275-50cce0d7c613&trk=flagship3_job_home_savedjobs)
    - hybrid pas kerja
- [Titippaket - Back End Developer - Jakarta](https://www.linkedin.com/jobs/view/2795906605/?refId=6416e2d2-13db-4d92-b275-50cce0d7c613&trk=flagship3_job_home_savedjobs)
    - on-site pas kerja
- [Virgo - Backend Engineer - Jakarta](https://www.linkedin.com/jobs/view/2804794939/?refId=6a8b0c66-5b76-4bcb-b275-7c1a9ce2dc9f)

## Singapura

- [Ninja Van - Software Engineer](https://jobs.lever.co/ninjavan/9efeba2b-4d12-4a5e-863d-e5f3564bb27f)
- [J&T Express Singapore - Senior Java Development Engineer](https://www.linkedin.com/jobs/view/2790557968/?refId=qMm64MYN2xNwRYcRV1u6tQ%3D%3D&trackingId=xX3SsYfZMWFYrhli108NmQ%3D%3D&trk=d_flagship3_company)
- [Grab - Software Engineer, Backend - Mobility, Automation and Platform Excellence, Deliveries and Experiences](https://www.linkedin.com/jobs/view/2774501443/?refId=9kFf03UEqpLtJJuwQ3f8nA%3D%3D&trackingId=iN3mFG0E%2Bc7%2FDOFhfWYAzQ%3D%3D&trk=d_flagship3_company)
- [Grab Financial Group - Software Engineer, Backend](https://www.linkedin.com/jobs/view/2773767761/?refId=9kFf03UEqpLtJJuwQ3f8nA%3D%3D&trackingId=jYmMTWgngoF5FoZ%2B%2FOaFig%3D%3D&trk=d_flagship3_company)
- [Shopee - Backend Software Engineer - Recommendation](https://www.linkedin.com/jobs/view/2794693772)

## Malaysia
- [Grab - Software Engineer, Backend - Petaling Jaya, Selangor](https://www.linkedin.com/jobs/view/2773767767/?refId=9kFf03UEqpLtJJuwQ3f8nA%3D%3D&trackingId=tJ%2B8d3tizDxV1BNosyftFA%3D%3D&trk=d_flagship3_company)

## Eropa

- [Microsoft - Software Engineer - Oslo, Norwegia](https://www.linkedin.com/jobs/view/2803044743/?alternateChannel=search&refId=l5ToF5YRujJg8R9P9Y4%2FEQ%3D%3D&trackingId=9dbtcdIk1hsX6kVc8kmuig%3D%3D&trk=d_flagship3_search_srp_jobs)
- [Picnic - Software Engineer - Amsterdam, Belanda](https://www.linkedin.com/jobs/view/2802934484/?refId=6e5f312a-42a6-41fe-be00-a2ba01c92c9f&trk=flagship3_job_home_savedjobs)
- [Booking.com - Back End Developer - Amsterdam, Belanda](https://www.linkedin.com/jobs/view/2773823084/?refId=6e5f312a-42a6-41fe-be00-a2ba01c92c9f&trk=flagship3_job_home_savedjobs)
- [Workday - Software Application Engineer - Muenchen, Jerman](https://www.linkedin.com/jobs/view/2795927329/?refId=6e5f312a-42a6-41fe-be00-a2ba01c92c9f&trk=flagship3_job_home_savedjobs)
- [Zenly - Software Engineer, Backend - Paris, Prancis](https://jobs.lever.co/zenly/008bd55c-85f6-4478-bc2e-241aa3df65cb)
- [GitLab - Backend Engineer, Monitor - Amsterdam, Belanda](https://www.linkedin.com/jobs/view/2781535240/?refId=KA9ZO7e1c6s5iyJ3fiPRLQ%3D%3D&trackingId=%2BW4aA6pARp4%2Ff8AR6iPoyA%3D%3D&trk=d_flagship3_company)
- [N26 - Senior Backend Engineer - Berlin, Jerman](https://www.linkedin.com/jobs/view/2793676931/?refId=4994d962-6e5a-4806-92b0-b78a6a115ee3&trk=flagship3_job_home_savedjobs)
- [GitLab - Backend Engineer, Database - Helsinki, Finlandia](https://www.linkedin.com/jobs/view/2802978478/?refId=hcNag2dDtS8coMyfFRcHqA%3D%3D&trackingId=uRr4VHU%2F8au9D4LBThVtPQ%3D%3D&trk=d_flagship3_company)
- [GitLab - Senior Backend Engineer, Pages, Create: Editor - Zurich, Swiss](https://www.linkedin.com/jobs/view/2802979370/?refId=hcNag2dDtS8coMyfFRcHqA%3D%3D&trackingId=O82FEFn4ALfPsoIL0nq7Fg%3D%3D)
- [bunq - Backend Engineer - Amsterdam, Belanda](https://www.linkedin.com/jobs/view/2761742285/?refId=frtXSq0JGvi8fwgp33qvIA%3D%3D&trackingId=N14ju1jaWs1LVbdYEDb74g%3D%3D&trk=d_flagship3_company)
- [Lunar - Software Engineer • Backend (CPH) - Copenhagen, Denmark](https://www.glassdoor.com/job-listing/software-engineer-backend-cph-lunar-JV_IC2218704_KO0,29_KE30,35.htm?jl=1007128471549&pos=102&ao=1136043&s=58&guid=0000017d41ca858688c27ed97ee6e55a&src=GD_JOB_AD&t=SR&vt=w&uido=10D197B9A7BDB6602CE8223D61C5F823&cs=1_e936a551&cb=1637486331485&jobListingId=1007128471549&jrtk=3-0-1fl0sl1deu3tf801-1fl0sl1dqu3gr800-e5619204bc13dcab-&ctt=1637486345144)
    - login dlu di Glassdoor
- [Zendesk - Software Engineer - Copenhagen, Denmark](https://www.glassdoor.com/job-listing/software-engineer-zendesk-JV_IC2218704_KO0,17_KE18,25.htm?jl=1007426764956&pos=108&ao=1136043&s=58&guid=0000017d41ca858688c27ed97ee6e55a&src=GD_JOB_AD&t=SR&vt=w&uido=10D197B9A7BDB6602CE8223D61C5F823&cs=1_8a62014b&cb=1637486331487&jobListingId=1007426764956&jrtk=3-0-1fl0sl1deu3tf801-1fl0sl1dqu3gr800-b6b82e5d89f21727-&ctt=1637486428848)
    - login dlu di Glassdoor
- [LEGO - Software Engineer - Copenhagen, Denmark](https://www.glassdoor.com/job-listing/software-engineer-copenhagen-lego-JV_IC2218704_KO0,28_KE29,33.htm?jl=1007163545305&pos=128&ao=1136043&s=58&guid=0000017d41ca858688c27ed97ee6e55a&src=GD_JOB_AD&t=SR&vt=w&uido=10D197B9A7BDB6602CE8223D61C5F823&cs=1_f1e58a85&cb=1637486331491&jobListingId=1007163545305&jrtk=3-0-1fl0sl1deu3tf801-1fl0sl1dqu3gr800-bd65e84bd186fb32-&ctt=1637486436251)
- [AWS - Software Development Engineer II, AWS S3 - Berlin, Jerman](https://www.glassdoor.com/job-listing/software-development-engineer-ii-aws-s3-aws-emea-sarl-germany-branch-JV_IC2622109_KO0,39_KE40,68.htm?jl=1007447887116&pos=110&ao=1136043&s=58&guid=0000017d41ce50849f04a1ba34b00624&src=GD_JOB_AD&t=SR&vt=w&uido=10D197B9A7BDB6602CE8223D61C5F823&cs=1_188c4c81&cb=1637486580130&jobListingId=1007447887116&jrtk=3-0-1fl0ssk5du3b0801-1fl0ssk5tu3di800-cc8118e5086c8b2f-&ctt=1637486631796)
- [Monzo - Senior Backend Engineer - Platform - London, Inggris](https://www.linkedin.com/jobs/view/2789867842/?refId=eB2WeXWJwG0saAS14VWu2g%3D%3D&trackingId=rAO5mrPAiIensYnUaO4HtA%3D%3D&trk=d_flagship3_company)
- [Revolut - Java Backend Engineer - Spanyol](https://www.linkedin.com/jobs/view/2691179026)
- [Google - Software Engineer, YouTube - Paris, Prancis](https://www.linkedin.com/jobs/view/2716142422/?refId=PSHoVGo%2B%2BBkdlJWW%2BBghjQ%3D%3D&trackingId=sj7PCv7mDi%2Fj1ZQBP4tnQw%3D%3D&trk=d_flagship3_company)
    - on-site pas kerja
- [Google - Software Engineer - Muenchen, Jerman](https://www.linkedin.com/jobs/view/2754095850/?refId=PSHoVGo%2B%2BBkdlJWW%2BBghjQ%3D%3D&trackingId=J5elIZYM3kvxXxxrGnfuhg%3D%3D&trk=d_flagship3_company)

## Amerika Serikat

- [Tinder - Backend Software Engineer - San Fransisco, CA](https://www.linkedin.com/jobs/view/2771454082/?refId=ukq8n1SyWWAkoJoUDOZ6zw%3D%3D&trackingId=9gsvop%2BBglRUs9Igpts%2FqA%3D%3D)
- [Lucid Motors - Software Engineer, Infotainment - Newark, CA](https://www.linkedin.com/jobs/view/2652447422/?refId=dff0be10-da5c-4626-a440-62c50191b824&trk=flagship3_job_home_savedjobs)
- [Red Hat - Software Engineer - Raleigh, NC](https://www.linkedin.com/jobs/view/2795925089/?refId=6e5f312a-42a6-41fe-be00-a2ba01c92c9f&trk=flagship3_job_home_savedjobs)
- [Splunk - Software Engineer - Boulder, CO](https://jobs.jobvite.com/careers/splunk/job/or91gfwB?__jvst=Job%20Board&__jvsd=Indeed)
- [Salesforce - Backend Software Development Engineer (All Levels) - Marketing Cloud - Denver, CO](https://salesforce.wd1.myworkdayjobs.com/en-US/External_Career_Site/job/Washington---Bellevue/Backend-Software-Development-Engineer--All-Levels----Marketing-Cloud_JR111651)
- [Workday - Software Engineer I or Software Engineer II - Backend - Boulder, CO](https://workday.wd5.myworkdayjobs.com/en-US/Workday/job/USA-CO-Boulder/Software-Engineer-I-or-Software-Engineer-II---Backend_JR-58799)
- [Atlassian - Back End Software Engineer, Event Platform - San Fransisco, CA](https://www.linkedin.com/jobs/view/2788373685/?refId=CHp0vEG0%2B9xtBYV1sd9RuQ%3D%3D&trackingId=uoQe%2FLT%2FtrWmg73VWoeDZQ%3D%3D&trk=d_flagship3_company)
- [Atlassian - Senior Back End Software Engineer, Bitbucket - Austin, TX](https://www.linkedin.com/jobs/view/2792474798/?refId=1U8DJyMI%2BE9FYvr5Zz%2BKGQ%3D%3D&trackingId=3ZTCrGxlJEFI7PbbFjOcwg%3D%3D&trk=d_flagship3_company)
- [Back End Software Engineer, Confluence Tailored Experiences - Mountain View, NC](https://www.linkedin.com/jobs/view/2792475736/?refId=1U8DJyMI%2BE9FYvr5Zz%2BKGQ%3D%3D&trackingId=h1J6HhdURXHU1Itqbs9vIw%3D%3D&trk=d_flagship3_company)
- [Robinhood - Software Engineer - Menlo Park, CA](https://www.linkedin.com/jobs/view/2369139647/?refId=ybaePpGeQVvBVOU1Bv87XA%3D%3D&trackingId=f0OQSJbWm4DR5nppMfhXZw%3D%3D&trk=d_flagship3_company)
- [Vultr - Solutions Engineer - West Palm Beach, FL](https://www.linkedin.com/jobs/view/2801726068/?refId=QIaWnhaI3G5qvL319LbgdA%3D%3D&trackingId=dXyieE%2FPsoxqseVNJCiKtw%3D%3D&trk=d_flagship3_company)
- [Dropbox - Software Engineer - San Fransisco, CA](https://www.linkedin.com/jobs/view/2731876610/?refId=6416e2d2-13db-4d92-b275-50cce0d7c613&trk=flagship3_job_home_savedjobs)
- [DocuSign - Software Engineer, Agreement Cloud Application - Seattle, WA](https://www.linkedin.com/jobs/view/2803923801/?refId=6416e2d2-13db-4d92-b275-50cce0d7c613&trk=flagship3_job_home_savedjobs)
- [Stripe - Backend / API Engineer, Stripe Terminal - Miami, FL](https://www.linkedin.com/jobs/view/2805582061/?refId=6a8b0c66-5b76-4bcb-b275-7c1a9ce2dc9f&trk=flagship3_job_home_savedjobs)

## Australia
- [Atlassian - Senior Backend Software Engineer - Sydney](https://www.linkedin.com/jobs/view/2769707089/?refId=1U8DJyMI%2BE9FYvr5Zz%2BKGQ%3D%3D&trackingId=Pkur4L5FH7%2FgBjGUIgSZig%3D%3D&trk=d_flagship3_company)
- [Red Hat - Software Engineer - Site Reliability - Melbourne, Victoria](https://www.linkedin.com/jobs/view/2806614831)
- [mx51 - Software Engineer (Golang)](https://www.linkedin.com/jobs/view/2785238944)

## Selandia Baru

- [Workday - Associate Software Application Engineer - Auckland](https://workday.wd5.myworkdayjobs.com/en-US/Workday/job/New-Zealand-Auckland/Associate-Software-Application-Engineer_JR-58583)
- [Lendi - Software Engineer CMS - Auckland](https://www.linkedin.com/jobs/view/2807414050/?alternateChannel=search&refId=Ia%2F2QDfgP1%2FAbVMccopyaw%3D%3D&trackingId=SQoyDXqUYTOvO0HbxvegCQ%3D%3D&trk=d_flagship3_search_srp_jobs)
- [GitLab - Backend Engineer, Verify: Runner - Auckland](https://www.linkedin.com/jobs/view/2810779870/?alternateChannel=search&refId=Ia%2F2QDfgP1%2FAbVMccopyaw%3D%3D&trackingId=uYwPy2boRbB3giGfY5cnZQ%3D%3D&trk=d_flagship3_search_srp_jobs)
